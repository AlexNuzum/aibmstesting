package tests;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.github.javafaker.Faker;
import com.github.javafaker.IdNumber;

import wrappers.WebDriverWrappers;

import java.io.FileWriter;
import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
public class ValidDataTest {
	private WebDriver driver;
	private Map < String, Object > vars;
	JavascriptExecutor js;
	public void setUp() {
		//System.setProperty("webdriver.gecko.driver","C:\\geckodriver.exe");
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		driver = new FirefoxDriver();
		//driver = new FirefoxDriver();
		//System.setProperty("phantomjs.binary.path","C:\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		//driver = new PhantomJSDriver();
		js = (JavascriptExecutor) driver;
		vars = new HashMap < String, Object > ();
	}
	public void tearDown() {
		driver.quit();
	}
	public void validData() {
		setUp();

		String testResults = "";
		Faker faker = new Faker();
		WebDriverWrappers wdw = new WebDriverWrappers(driver);
		

		driver.get("http://aibms-mbdev.suredatum.com/");
		driver.manage().window().maximize();
		testResults += "Successfully loaded webpage.\n";

		int productType = getRandomNumber(1,4);

		testResults += wdw.findElement(By.id("documentIdPreparedIndicator"), "#documentIdPreparedIndicator");
		
		System.out.println(productType);
		
		if(productType == 1) {
			testResults += wdw.clickElement(By.cssSelector(".nav-online"),".nav-online");
		}else if(productType == 2) {
			testResults += wdw.clickElement(By.cssSelector(".nav-in-store"),".nav-in-store");
		}else {
			testResults += wdw.clickElement(By.cssSelector(".nav-on-the-go"),".nav-on-the-go");
		}

		int country = getRandomNumber(1,3);
		testResults += wdw.clickElement(By.cssSelector(".country-button:nth-child("+country+")"), ".country-button:nth-child("+country+")");
		
		if(productType == 1 || productType == 3) {
			testResults += wdw.clickElement(By.cssSelector(".product:nth-child(1) .active"), ".product:nth-child(1) .active");
		}else {
			int product = getRandomNumber(1,3);
			testResults += wdw.clickElement(By.cssSelector(".product:nth-child("+product+") .active"), ".product:nth-child("+product+") .active");
		}

		testResults += wdw.typeInElement(By.id("given-name"),"#given-name",faker.hitchhikersGuideToTheGalaxy().character());

		testResults += wdw.typeInElement(By.id("email"), "#email", faker.bothify("????##??@?????.????"));

		testResults += wdw.typeInElement(By.id("phone"), "#phone", Integer.toString(faker.number().numberBetween(1000000000, 2147483647)));

		testResults += wdw.clickElement(By.id("FEFields-MarketingConsent"), "#FEFields-MarketingConsent");

		testResults += wdw.clickElement(By.cssSelector(".next"), ".next");

		testResults += wdw.findElement(By.id("optionListsDownloadedIndicator"), "#optionListsDownloadedIndicator");

		int businessType = getRandomNumber(0,5);
		testResults += wdw.clickElement(By.id("select-BusinessType"), "#select-BusinessType");
		testResults += wdw.clickElement(By.id("option-"+businessType+"-BusinessType"), "#option-"+businessType+"-BusinessType");

		testResults += wdw.typeInElement(By.id("company legalName"), "#company legalName", faker.company().name());

		if(businessType == 0 || businessType == 3) {
			testResults += wdw.typeInElement(By.id("company number"), "#company number", Integer.toString(faker.number().numberBetween(0, 2147483647)));
		}else {
			wdw.isEditable(By.id("company number"), "#company number");
		}
		
		boolean isVatRegistered = getRandomBoolean();
		if(isVatRegistered) {
			testResults += wdw.clickElement(By.id("yes-FEFields-hasVatRegistered"), "#yes-FEFields-hasVatRegistered");
			testResults += wdw.typeInElement(By.id("company vat"), "#company vat", Integer.toString(faker.number().numberBetween(10000, 99999)));
		}else {
			testResults += wdw.clickElement(By.id("no-FEFields-hasVatRegistered"), "#no-FEFields-hasVatRegistered");
			wdw.isEditable(By.id("company vat"), "#company vat");
		}

		testResults += wdw.clickElement(By.id("date-BusinessStartDate"), "#date-BusinessStartDate");
		testResults += wdw.clickElement(By.cssSelector(".dp-prev-month:nth-child(1)"), ".dp-prev-month:nth-child(1)");
		testResults += wdw.typeInElement(By.id("address-line1"), "#address-line1", faker.address().buildingNumber());
		testResults += wdw.typeInElement(By.id("address-line2"), "#address-line2", faker.address().streetAddress());

		if(productType == 1) {
			testResults += wdw.typeInElement(By.id("input-Website"), "#input-Website", faker.bothify("https://?????.???/"));
		}
		
		testResults += wdw.typeInElement(By.id("city"), "#city", faker.address().cityName());
		testResults += wdw.typeInElement(By.id("country-name"), "#country-name", faker.address().state());
		testResults += wdw.clickElement(By.id("select-Address-Country"), "#select-Address-Country");
		
		int companyCountry = getRandomNumber(0,2);
		testResults += wdw.clickElement(By.id("option-"+companyCountry+"-Address-Country"), "#option-"+companyCountry+"-Address-Country");

		testResults += wdw.typeInElement(By.id("postal-code"), "#postal-code", faker.bothify("?##?###"));

		testResults += wdw.clickElement(By.cssSelector(".next"), ".next");

		testResults += wdw.findElement(By.id("optionListsDownloadedIndicator"), "#optionListsDownloadedIndicator");

		testResults += wdw.clickElement(By.cssSelector(".add-director-button"), ".add-director-button");

		int title = getRandomNumber(0,6);
		testResults += wdw.clickElement(By.id("select-BusinessContacts-0-Title"), "#select-BusinessContacts-0-Title");
		testResults += wdw.clickElement(By.id("option-"+title+"-BusinessContacts-0-Title"), "#option-"+title+"-BusinessContacts-0-Title");

		testResults += wdw.typeInElement(By.id("name"), "#name", faker.name().firstName());

		testResults += wdw.typeInElement(By.id("lastName"), "#lastName", faker.name().lastName());

		testResults += wdw.typeInElement(By.id("email"), "#email", faker.bothify("????##??@?????.????"));

		testResults += wdw.clickElement(By.id("date-BusinessContacts-0-DateOfBirth"), "#date-BusinessContacts-0-DateOfBirth");

		testResults += wdw.clickElement(By.cssSelector(".dp-nav-header-btn"), ".dp-nav-header-btn");

		testResults += wdw.clickElement(By.cssSelector(".dp-calendar-secondary-nav-left"), ".dp-calendar-secondary-nav-left");
		testResults += wdw.clickElement(By.cssSelector(".dp-calendar-secondary-nav-left"), ".dp-calendar-secondary-nav-left");

		testResults += wdw.clickElement(By.cssSelector(".dp-months-row:nth-child(1) > .dp-calendar-month:nth-child(1)"), ".dp-months-row:nth-child(1) > .dp-calendar-month:nth-child(1)");

		testResults += wdw.clickElement(By.cssSelector(".dp-calendar-week:nth-child(2) > .dp-calendar-day:nth-child(2)"), ".dp-calendar-week:nth-child(2) > .dp-calendar-day:nth-child(2)");

		testResults += wdw.typeInElement(By.id("contact phone"), "#contact phone", Integer.toString(faker.number().numberBetween(1000000000, 2147483647)));

		int nationality = getRandomNumber(0,251);
		testResults += wdw.clickElement(By.id("select-BusinessContacts-0-Nationality"), "#select-BusinessContacts-0-Nationality");
		testResults += wdw.clickElement(By.id("option-"+nationality+"-BusinessContacts-0-Nationality"), "#option-"+nationality+"-BusinessContacts-0-Nationality");

		boolean isBeneficialOwner = getRandomBoolean();
		if(isBeneficialOwner) {
			testResults += wdw.clickElement(By.id("yes-BusinessContacts-0-BeneficialOwner"), "#yes-BusinessContacts-0-BeneficialOwner");
			int percentage = getRandomNumber(25,101);
			testResults += wdw.typeInElement(By.id("input-BusinessContacts-0-BeneficialOwnerPercentage"), "#input-BusinessContacts-0-BeneficialOwnerPercentage", Integer.toString(getRandomNumber(25,101)));
		}else {
			testResults += wdw.clickElement(By.id("no-BusinessContacts-0-BeneficialOwner"), "#no-BusinessContacts-0-BeneficialOwner");
			wdw.isEditable(By.id("input-BusinessContacts-0-BeneficialOwnerPercentage"), "#input-BusinessContacts-0-BeneficialOwnerPercentage");
		}

		testResults += wdw.typeInElement(By.id("address-line1"), "#address-line1", faker.address().buildingNumber());

		testResults += wdw.typeInElement(By.id("address-line2"), "#address-line2", faker.address().streetAddress());

		testResults += wdw.typeInElement(By.id("city"), "#city", faker.address().city());

		testResults += wdw.typeInElement(By.id("country-name"), "#country-name", faker.address().state());

		int countryNationality = getRandomNumber(0,251);
		testResults += wdw.clickElement(By.id("select-BusinessContacts-0-ResidentialAddress-Country"), "#select-BusinessContacts-0-ResidentialAddress-Country");
		testResults += wdw.clickElement(By.id("option-"+countryNationality+"-BusinessContacts-0-ResidentialAddress-Country"), "#option-"+countryNationality+"-BusinessContacts-0-ResidentialAddress-Country");

		testResults += wdw.typeInElement(By.id("postal-code"), "#postal-code", faker.bothify("?##?###"));

		testResults += wdw.clickElement(By.cssSelector(".next"), ".next");

		testResults += wdw.findElement(By.id("optionListsDownloadedIndicator"), "#optionListsDownloadedIndicator");

		int mccID = getRandomNumber(0,947);
		testResults += wdw.clickElement(By.id("select-FEFields-MccId"), "#select-FEFields-MccId");
		testResults += wdw.clickElement(By.id("option-"+mccID+"-FEFields-MccId"), "#option-"+mccID+"-FEFields-MccId");

		testResults += wdw.typeInElement(By.id("input-ATV"), "#input-ATV", Integer.toString(faker.number().numberBetween(0, 2147483647)));

		testResults += wdw.typeInElement(By.id("input-CardTurnover"), "#input-CardTurnover", Integer.toString(faker.number().numberBetween(0, 2147483647)));

		testResults += wdw.typeInElement(By.id("input-AnnualTurnover"), "#input-AnnualTurnover", Integer.toString(faker.number().numberBetween(0, 2147483647)));

		boolean depositsAccepted = getRandomBoolean();
		if(depositsAccepted) {
			testResults += wdw.clickElement(By.id("yes-DepositsAccepted"), "#yes-DepositsAccepted");
		}else {
			testResults += wdw.clickElement(By.id("no-DepositsAccepted"), "#no-DepositsAccepted");
		}

		testResults += wdw.typeInElement(By.id("bank name"), "#bank name", faker.company().name());

		testResults += wdw.typeInElement(By.id("account holder"), "#account holder", faker.funnyName().name());

		testResults += wdw.typeInElement(By.id("input-BankDetails-IBAN"), "#input-BankDetails-IBAN", "IE64IRCE92050112345678");

		js.executeScript("window.scrollTo(0,1454.54541015625)");

		testResults += wdw.clickElement(By.cssSelector(".next"), ".next");
		
		
		System.out.println(testResults);
		try {
			FileWriter myWriter = new FileWriter("output.txt");
			myWriter.write(testResults);
			myWriter.close();
			System.out.println("Successfully wrote to file");
		} catch (IOException e) {
			System.out.println("Failed to write to file");
			e.printStackTrace();
		}
		tearDown();
	}
	
	public static boolean getRandomBoolean() {
		return Math.random() < 0.5;
	}
	
	public int getRandomNumber(int min, int max) {
	    return (int) ((Math.random() * (max - min)) + min);
	}
}