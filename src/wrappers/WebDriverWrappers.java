package wrappers;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

public class WebDriverWrappers {
	WebDriver driver;
	
	public WebDriverWrappers(WebDriver webdriver) {
		driver = webdriver;
	}
	
	public String clickElement(By element,String selector) {
		String message = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));
			driver.findElement(element).click();
			message = "Successfully clicked '"+selector+"'.\n";
		}catch(org.openqa.selenium.NoSuchElementException e) {
			message = "Element '"+selector+"' could not be found.\n";
		}
		System.out.println(message);
		return message;
	}
	
	public String clickElement(By element,String selector, JavascriptExecutor js) {
		String message = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));
			//driver.findElement(element).click();
			js.executeScript("arguments[0].click();", element);
			message = "Successfully clicked '"+selector+"'.\n";
		}catch(org.openqa.selenium.NoSuchElementException e) {
			message = "Element '"+selector+"' could not be found.\n";
		}
		System.out.println(message);
		return message;
	}
	
	public String findElement(By element, String selector) {
		String message = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(element));
			message = "Element '"+selector+"' located.\n";
		}catch(org.openqa.selenium.NoSuchElementException e) {
			message = "Element '"+selector+"' could not be found.\n";
		}
		System.out.println(message);
		return message;
	}

	public String typeInElement(By element, String selector,String value) {
		String message = null;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(element));
			driver.findElement(element).sendKeys(value);
			message = "Successfully typed '"+value+"' into '"+selector+"'.\n";
		}catch(org.openqa.selenium.NoSuchElementException e) {
			message = "Element '"+selector+"' could not be found.\n";
		}
		return message;
	}
	
	public String isEditable(By element, String selector) {
		WebElement elem = driver.findElement(element);
		Boolean isEditable = elem.isEnabled() && elem.getAttribute("readonly") == null;
		return "Is Editable: " + isEditable;
	}
}
